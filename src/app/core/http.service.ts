import { Injectable } from '@angular/core';
import {Response, RequestOptionsArgs, Request, Headers, Http} from '@angular/http';
//import { Cookie } from "ng2-cookies/ng2-cookies";
import { Observable } from 'rxjs/Rx';

//import { SessionService } from "./SessionService";
//import { ApiSettings } from '../../api.settings'
//import { AuthMiddlewareService } from "../../shared/middlewares/AuthMiddlewareService";
//import { ResponseMessageService } from "../../shared/services/ResponseMessage.service";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { AuthService } from './auth.service';
/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class HttpService {
  /**
   * The token
   * @type {string}
   * @private
   */
  protected _token: string = "";
  /**
   * The local token auth key
   * @type {string}
   */
  protected TOKEN_STORE_KEY: string = "_app_session_key";

  protected TOKEN_NAME = "app_access_token";

  constructor(private http: Http, public toastr: ToastsManager, private auth: AuthService) {
    this.setAuthTokenFromCookie();
  }

  protected getOptions(options?:RequestOptionsArgs):RequestOptionsArgs{
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' +localStorage.getItem('app_access_token') );

    if(options){
      options['headers'] = headers;
      return options;
    }
    return  {
      headers:headers
    }
  }
  /**
   * Get token from cookie
   * @returns {string}
   */
  protected getTokenFromCookie(): any {
    //return Cookie.get(this.TOKEN_STORE_KEY);
    return;
  }

  protected setAuthTokenToHeader(): void {

  }

  protected setAuthTokenFromCookie(): void {
    this._token = this.getTokenFromCookie();
    this.setAuthTokenToHeader();
  }

  getAppAccessTokenName(): string {
    return this.TOKEN_NAME;
  }



  protected __request(request: Observable<any>): Observable<any> {

    return request.map((res: Response) => {
      //if (res.headers.get(this.TOKEN_NAME)) {
      //  localStorage.setItem(this.TOKEN_NAME, res.headers.get(this.TOKEN_NAME));
      //  this.session.setToken(res.headers.get(this.TOKEN_NAME));
      //}
      return res;
    }).catch(this.catchError.bind(this));
  }

  private catchError(res: Response) {
     if (res.status == 500) {
         //this.responsemessage.showMessage();
       this.toastr.error('500 server error');

     }
  else if (res.status == 400) {
      this.toastr.error('Logging out......');
      //window.location.href = this.apiSettings.url.logout;
      //this.auth.logout();
      //localStorage.removeItem(this.getAppAccessTokenName());
    }
    else if (res.status == 401) {
      this.toastr.error('You are unauthorized');
      this.auth.logout();

      //window.location.href = this.apiSettings.url.logout;
      //localStorage.removeItem(this.getAppAccessTokenName());
      // this.setUser(null);
    }
    else if (res.status == 402) {
      this.toastr.error('Payment Required');
    }
    else if (res.status == 403) { //when do not have sufficient permission
      this.toastr.error('Forbidden');
    }
    else if (res.status == 404) {
      this.toastr.error('Resource not found');
    }
    throw res;
    //return res;
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return this.__request(this.http.request(url, this.getOptions()));
  }
  /**
   * Performs a request with `get` http method.
   */
  get(url: string, options?: RequestOptionsArgs): Observable<Response> {

    return this.__request(this.http.get(url, this.getOptions(options)));
  }
  /**
   * Performs a request with `post` http method.
   */
  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.__request(this.http.post(url, body, this.getOptions()));
  }
  /**
   * Performs a request with `put` http method.
   */
  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.__request(this.http.put(url, body, this.getOptions()));

  }
  /**
   * Performs a request with `delete` http method.
   */
  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.__request(this.http.delete(url, this.getOptions()));
  }
  /**
   * Performs a request with `patch` http method.
   */
  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.__request(this.http.patch(url, body, this.getOptions()));
  }
  /**
   * Performs a request with `head` http method.
   */
  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.__request(this.http.head(url, this.getOptions()));
  }
  /**
   * Performs a request with `options` http method.
   */
  options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.__request(this.http.options(url, this.getOptions()));
  }



}