import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import {HttpService} from "./http.service";

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class RestService {

  /**
   * Creates a new NameListService with the injected Http.
   * @param {HttpService} http - The injected Http.
   * @constructor
   */
  constructor(private http:HttpService) {

  }

  /**
   * @param res
   * @returns {Observable}
   */
  private catchError(res:Response):Observable<any> {
    let json = null;
    try {
      json = res.json();
      json.status.httpStatus = res.status;
    } catch (e) {
      json = {status: {httpStatus: res.status, text: res.statusText}};
      if(res.status=422){
        json = {status: {httpStatus: res.status, text: res.statusText}, errors: JSON.parse(res['_body'])};
      }
    }
    return Observable.throw(json);
  }

  request(url:string, method?:string, data?:any):Observable<any> {
    let observe:Observable<any> = null;
    switch (method) {
      case 'get':
      case 'GET':
        observe = this.http.get(url,data);
        break;
      case 'post':
      case 'POST':
        observe = this.http.post(url, data);
        break;
      case 'delete':
      case 'DELETE':
        observe = this.http.delete(url);
        break;
      case 'patch':
      case 'PATCH':
        observe = this.http.patch(url, data);
        break;
      case 'options':
      case 'OPTIONS':
        observe = this.http.options(url, data);
        break;
      case 'put':
      case 'PUT':
        observe = this.http.put(url, data);
        break;
      default:
        observe = this.http.request(url);
    }
    return observe.map((d:any)=> {
      return d.json();
    });
  }

  /**
   *
   * @param url
   * @param params
   * @returns {Observable}
   */
  list(url:string, params?:any):Observable<any> {
    return this.http.get(url)
        .map((res:Response)=>res.json())
        .catch(this.catchError);
  }

  /**
   *
   * @param url
   * @param obj
   * @returns {Observable}
   */
  createItem(url:string, obj:any):Observable<any> {
    return this.http.post(url, obj)
        .map((res:Response)=> {
          return res.json();
        }).catch(this.catchError);
  }

  updateItem(url:string, obj:any):Observable<any> {
    return this.http.put(url, obj)
        .map((res:Response)=> {
          return res.json();
        }).catch(this.catchError);
  }

  patchItem(url:string, obj:any):Observable<any> {
    return this.http.patch(url, obj)
        .map((res:Response)=> {
          return res.json();
        }).catch(this.catchError);
  }

  destroy(url:string, obj?:any):Observable<any> {
    return this.http.delete(url).map((res:Response)=> {
      return res.json();
    }).catch(this.catchError);
  }

  getItem(url:string):Observable<any> {
    return this.http.get(url)
        .map((res:Response)=> {
          return res.json();
        }).catch(this.catchError);
  }


  options(url:string):Observable<any> {
    return this.http.get(url)
        .map((res:Response)=> {
          return res.json();
        }).catch(this.catchError);
  }

}