/**
 * Created by ruban on 4/18/2017.
 */
import {ToastOptions} from 'ng2-toastr';

export  class CustomToastOption extends ToastOptions {
    animate = 'flyRight';
    enableHTML = true;
}