import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Subscription }   from 'rxjs/Subscription';

import { environment } from '../../../environments/environment';
import { AuthService } from '../auth.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import {AjaxSpinnerService} from '../../shared/ajax-spinner.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  disableForm:boolean = false;
  credentials:any = {"username":"","password":""};
  loginSubscription: Subscription;

  constructor( protected authService: AuthService,  public toastr: ToastsManager, vcr: ViewContainerRef, public router: Router
      , private spinner: AjaxSpinnerService
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  login(){

    //let spinner = this.spinner.showSpinner('login');
    this.disableForm = true;
    this.loginSubscription = this.authService.login(this.credentials).subscribe((d)=>{
      ///perform redirection
      if(this.authService.isLoggedIn()){
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default

        let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '';
        // Set our navigation extras object
        // that passes on our global query params and fragment
        let navigationExtras: NavigationExtras = {
          preserveQueryParams: true,
          preserveFragment: true
        };
        this.disableForm = false;
        //spinner.hide();
        // Redirect the user
        this.router.navigate([redirect], navigationExtras);
      }
    },(e) => {
      this.disableForm = false;
      //spinner.hide();
      //show validation error msg
      this.toastr.error("Invalid Username/password");
    });
  }

  ngOnDestroy(){
  }

}
