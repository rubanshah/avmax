import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {Response, RequestOptionsArgs, Request, Headers, Http} from '@angular/http';


import { RestService } from './rest.service';
import { environment } from '../../environments/environment';

//remaining to work on refresh token, if possible get userinfo when app boots

@Injectable()
export class AuthService {
  protected LOGIN_ENDPOINT_URL = 'login';

  protected ACCESS_TOKEN_KEY = 'app_access_token';
  protected REFRESH_TOKEN_KEY = 'app_refresh_token';
  protected PROFILE_KEY = 'profile';

  public redirectUrl:string;

  constructor(protected http: Http, protected router: Router) {

  }

  login(credentials){
    return this.http.post(environment.API_URL+'/'+this.LOGIN_ENDPOINT_URL,credentials).map(d => d.json()).map((d)=>{
      console.log(d);
      this.setToken(d.access_token);
      this.setUser(d.user);
      return d;
    },(e)=>{

    });

  }

  isLoggedIn(){
    return this.getToken()?true:false;
  }

  getToken()
  {
    return localStorage.getItem(this.ACCESS_TOKEN_KEY);
  }

  setToken(token){
    localStorage.setItem(this.ACCESS_TOKEN_KEY,token);
  }

  //param userobject eloquent user model
  setUser(data){
    let user = {};
    user['id']=data.id;
    user['username']=data.username;
    user['email']=data.email;
    user['first_name']=data.first_name;
    user['last_name']=data.last_name;
    localStorage.setItem(this.PROFILE_KEY,JSON.stringify(user));
  }

  getUser(){
    return JSON.parse(localStorage.getItem(this.PROFILE_KEY));
  }

  updateUser(){

  }

  refreshToken(){

  }

  /**
   * remaining to send ajax request and delete token from server;
   */
  logout(): void {
    localStorage.removeItem(this.ACCESS_TOKEN_KEY);
    localStorage.removeItem(this.PROFILE_KEY);
    this.router.navigate(['/login']);
  }

}
