import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ModalModule } from '../modal/modal.module';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {CustomToastOption} from './customToastOption';
import {ToastOptions} from 'ng2-toastr';
import { BusyModule } from 'angular2-busy';

import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { RestService } from './rest.service';
import { HttpService } from './http.service';
import { AuthGuard } from './auth-guard.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ModalModule,
    ToastModule.forRoot(),
    BusyModule
  ],
  declarations: [NavComponent, LoginComponent],
  exports: [NavComponent, LoginComponent, ModalModule, ToastModule],
  providers:[{provide:ToastOptions, useClass: CustomToastOption},AuthService, RestService, HttpService, AuthGuard]
})
export class CoreModule { }
