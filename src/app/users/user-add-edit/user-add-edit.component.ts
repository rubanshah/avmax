import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { GeneralService } from '../../shared/general.service';
import { UserService } from '../user.service';
import { User } from '../model/user.model';


@Component({
  selector: 'app-user-add-edit',
  templateUrl: './user-add-edit.component.html',
  styleUrls: ['./user-add-edit.component.css']
})
export class UserAddEditComponent implements OnInit {

  @Output()
  cancel: EventEmitter<any> = new EventEmitter();

  @Input()
      id: number; //input entity id that is userid

  user:User;

  constructor(public generalService: GeneralService, public userService: UserService) {
  }


  ngOnInit() {
    this.user = new User();
    if(this.id){
      this.getUser();
    }
  }

  getUser(){
    this.userService.get(this.id).subscribe((d)=>{
      this.user = new User(d);
      console.log(this.user);
    },(e)=>{
      console.log(e);
    });
  }

  create(){

  }

  update(){

  }


  formCancel(){
    this.cancel.emit();
  }

  ngOnDestroy(){
  }


}
