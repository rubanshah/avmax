import { Injectable } from '@angular/core';
import { RequestOptionsArgs } from '@angular/http';


import {RestService} from '../core/rest.service';
import {environment} from '../../environments/environment';

@Injectable()
export class UserService {
  protected resourceEndPoint = 'users';

  constructor(protected http: RestService) { }

  getUsers(options){
    let url = environment.API_URL + '/' + this.resourceEndPoint;
    options={search:options};
    return this.http.request(url,'get',options);
  }

  get(id){
    let url = environment.API_URL + '/' + this.resourceEndPoint+'/'+id;
    return this.http.getItem(url);
  }

}
