import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from '../layout/layout.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';


const usersRoutes: Routes = [

  { path: '', component: LayoutComponent, children: [
    { path: 'users', component: UserListComponent },
    { path: 'user/:id', component: UserDetailComponent },
  ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class UserRoutingModule { }
