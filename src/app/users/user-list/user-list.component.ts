import { Component, OnInit, ViewChild, AfterViewInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

import { DatatableComponent } from "@swimlane/ngx-datatable";

import { UserService } from '../user.service';
import { Page, PageResponseInterface } from '../../shared/page';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  columns:Array<any> = [];
  users:Array<any> = [];

  page:Page = new Page();

  options = {page:0};
  loadingIndicator: boolean = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  @ViewChild('userNameCell') public userNameCell: TemplateRef<any>;
  @ViewChild('userRolesCell') public userRolesCell: TemplateRef<any>;

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {

    this.columns = [
      { prop: 'username' },
      { prop: 'name', cellTemplate: this.userNameCell },
      { prop: 'email' },
      { prop: 'designation' },
      { prop: 'role', cellTemplate: this.userRolesCell }
    ];

    this.setPage({offset:0});

}

  getUsers(){
    this.loadingIndicator = true;
    this.userService.getUsers(this.options).subscribe((d)=>{

      this.users = d.data;

      this.handlePagination(d);
      this.loadingIndicator =false;
    });
  }

  handleRowClick($event){
    let userId = $event.row.id;
    this.router.navigate(['/user', userId]);
  }


  setPage(pageInfo){
    this.page.pageNumber = pageInfo.offset;
    this.options.page = this.page.pageNumber+1;
    this.getUsers();
  }

  handlePagination(d:PageResponseInterface){
    this.page.totalElements = d.total;
    this.table.recalculate();
  }

}
