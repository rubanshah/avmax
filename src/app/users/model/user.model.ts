import { BaseEntity } from '../../shared/model/base-entity';
import { GeneralService } from '../../shared/general.service';
import * as _ from 'lodash';

export class User implements BaseEntity{
    static CONS_STATUS_UNVERIFIED = 0;
    static CONS_STATUS_VERIFIED = 1;
    static CONS_STATUS_SUSPENDED = 2;

    id:number=null;
    username: string;
    first_name: string;
    last_name: string;
    email: string;
    phone: number;
    status: number;
    address:string;
    country_code: string;
    city: string ;
    state: string;
    zip: string;
    image: string;
    designation: string;
    roles: Array<Object>;

    constructor(obj=null){
        this.status = User.CONS_STATUS_UNVERIFIED;
        if(obj){
            this.initUser(obj);
        }
    }

    initUser(obj){
        this.id = obj.id;
        this.username = obj.username;
        this.first_name = obj.first_name;
        this.last_name = obj.last_name;
        this.email = obj.email;
        this.phone = obj.phone;
        this.status = obj.status;
        this.address = obj.address;
        this.country_code = obj.country_code;
        this.city = obj.city;
        this.state = obj.state;
        this.zip = obj.zip;
        this.image = obj.image;
        this.designation = obj.designation;
        this.roles = obj.roles;
    }

    getName(){
        return this.first_name +' '+ this.last_name;
    }

    getAddress(){
        return this.address+', '+this.city+', '+this.state+', '+this.getCountryName();
    }

    getCountryName(){
        return new GeneralService().getCountryNameByCode(this.country_code);
    }

}