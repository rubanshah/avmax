import { Component, OnInit, OnDestroy, ComponentFactoryResolver } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription }   from 'rxjs/Subscription';
import { Observable }   from 'rxjs/Observable';

import { ModalService } from '../../modal/modal.service';
import { UserAddEditComponent } from '../user-add-edit/user-add-edit.component';

import { AppModule } from '../../app.module';
import { UserService } from '../user.service';
import { User } from '../model/user.model';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
})
export class UserDetailComponent implements OnInit, OnDestroy {

  private subscriptions:Subscription[] = [];
  protected id: number;
  public user:User;

  constructor( public route: ActivatedRoute,  protected userService: UserService, private modalService: ModalService, private componentFactoryResolver: ComponentFactoryResolver) {

  }

  ngOnInit() {
    this.route.params.subscribe((p)=>{
      this.id = p.id;
      this.getUser();
    });
  }

  getUser(){
    this.userService.get(this.id).subscribe((d)=>{
      this.user = new User(d);
    },(e)=>{
      console.log(e);
    });
  }

   edit(){
    let userAddEditComponent = this.modalService.create(AppModule, UserAddEditComponent, {id:this.id}).subscribe((c)=> {
      console.log(c);
      c['_component'].cancel.subscribe(()=> {
        c.destroy();
        this.modalService.bsModal.hide();
      });
    });
    this.subscriptions.push(userAddEditComponent);
  }


  ngOnDestroy(){
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
