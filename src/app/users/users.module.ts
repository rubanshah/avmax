import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ng2-bootstrap/modal';
import { TabsModule } from 'ng2-bootstrap/tabs'

import { UserRoutingModule } from './users-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserAddEditComponent } from './user-add-edit/user-add-edit.component';

import { UserService } from './user.service';
import { UserStatisticsComponent } from './user-statistics/user-statistics.component';

import { SharedModule } from '../shared/shared.module';
import { GeneralService } from '../shared/general.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule,
    NgxDatatableModule,
    ModalModule,
    TabsModule.forRoot(),
    SharedModule
  ],
  declarations: [UserListComponent, UserDetailComponent, UserAddEditComponent, UserStatisticsComponent],
  providers: [ UserService, GeneralService ],
  exports: [UserAddEditComponent],
  entryComponents:[UserAddEditComponent]
})
export class UsersModule { }
