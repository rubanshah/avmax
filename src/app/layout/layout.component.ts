import { Component, OnInit } from '@angular/core';

import { AuthService } from '../core/auth.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit() {
  }

  logout(){
    this.authService.logout();
  }

  toggleNav(){
    var el = document.getElementById('main-body');
    if(el.classList.contains('nav-md')){
      document.getElementById('main-body').className='nav-sm';
    }else{
      document.getElementById('main-body').className='nav-md';
    }
  }
}
