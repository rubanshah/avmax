import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { BsDropdownModule } from 'ng2-bootstrap';
//import { ModalModule } from './modal/modal.module';


import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { UsersModule } from './users/users.module';
import { CustomersModule } from './customers/customers.module';

import { PostsModule } from './posts/posts.module';

import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';

import  {AjaxSpinnerService} from './shared/ajax-spinner.service';


@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,

    BsDropdownModule.forRoot(),

    AppRoutingModule,
    CoreModule,
    DashboardModule,
    PostsModule,
    UsersModule,
    CustomersModule
    //ModalModule
  ],
  providers: [AjaxSpinnerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
