import { Component, OnInit, ViewChild, ViewContainerRef, Injector, ComponentFactoryResolver } from '@angular/core';

import { ModalDirective } from 'ng2-bootstrap/modal';
import { ModalService } from '../modal.service';

@Component({
  selector: 'app-modal-placeholder',
  templateUrl: './modal-placeholder.component.html',
  styleUrls: ['./modal-placeholder.component.css']
})
export class ModalPlaceholderComponent implements OnInit {

  @ViewChild("modalplaceholder", {read: ViewContainerRef}) viewContainerRef;
  @ViewChild("modal") public modal: ModalDirective;

  constructor(private modalService: ModalService, private injector: Injector, private resolver: ComponentFactoryResolver ) { }

  ngOnInit(): void {
    this.modalService.regitsterBsModalViewContainerRef(this.modal);
    this.modalService.registerViewContainerRef(this.viewContainerRef);
    this.modalService.registerInjector(this.injector);
    this.modalService.registerResolver(this.resolver);
  }

}
