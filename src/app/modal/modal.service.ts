//reference http://blog.brecht.io/Modals-in-angular2/

import { Component, ViewChild, OnInit, ViewContainerRef, Compiler, ReflectiveInjector, Injectable, Injector, ComponentRef, ComponentFactory, ComponentFactoryResolver } from '@angular/core';
import { Observable, Subject, BehaviorSubject, ReplaySubject } from 'rxjs/Rx';

import { ModalDirective } from 'ng2-bootstrap/modal';

@Injectable()
export class ModalService {
  public bsModal: any;
  private vcRef: ViewContainerRef;
  private injector: Injector;
  public activeInstances: number = 0;

  activeInstances$: Subject<number> = new Subject();
  modalRef: ComponentRef<any>[] = [];
  private resolver: ComponentFactoryResolver;

  constructor( private compiler: Compiler) {
  }

  regitsterBsModalViewContainerRef(vcRef: ModalDirective){
  this.bsModal = vcRef;
  }

  registerViewContainerRef(vcRef: ViewContainerRef): void {
    this.vcRef = vcRef;
  }

  registerInjector(injector: Injector): void {
    this.injector = injector;
  }

  create1<T>(module: any, component: any, parameters?: Object): Observable<ComponentRef<T>> {
    this.bsModal.show();

    let componentRef$ = new ReplaySubject();
    this.compiler.compileModuleAndAllComponentsAsync(module).then((factory) => {
      let componentFactory = factory.componentFactories.filter(item => item.componentType === component)[0];
      const childInjector = ReflectiveInjector.resolveAndCreate([], this.injector);
      let componentRef = this.vcRef.createComponent(componentFactory, 0, childInjector);
      Object.assign(componentRef.instance, parameters); //pass the @Input parameters to the instance
      this.activeInstances ++;
      componentRef.instance["componentIndex"] = this.activeInstances;

      componentRef.instance["destroy"] = () => {
        this.activeInstances --;
        componentRef.destroy();
      };

      componentRef.onDestroy(()=>{
        this.activeInstances --;
      });
      componentRef$.next(componentRef);
      componentRef$.complete();
    });
    return <Observable<ComponentRef<T>>> componentRef$.asObservable();
  }

//new jit and aot support
  public registerResolver(resolver: ComponentFactoryResolver) {
    this.resolver = resolver;
  }

  create<T>( module: any,component: any, parameters?: Object): Observable<ComponentRef<T>> {
    this.bsModal.show();

    const componentRef$ = new ReplaySubject();

    const injector = ReflectiveInjector.fromResolvedProviders([], this.vcRef.parentInjector),
        factory = this.resolver.resolveComponentFactory(component),
        componentRef = factory.create(injector);

    this.vcRef.insert(componentRef.hostView);

    Object.assign(componentRef.instance, parameters); // pass the @Input parameters to the instance

    this.activeInstances++;
    this.activeInstances$.next(this.activeInstances);

    componentRef.instance['componentIndex'] = this.activeInstances;
    componentRef.instance['destroy'] = () => {
      this.activeInstances--;
      this.activeInstances = Math.max(this.activeInstances, 0);

// remove modal instance from active instances array
      const idx = this.modalRef.indexOf(componentRef);
      if (idx > -1) {
        this.modalRef.splice(idx, 1);
      }

      this.activeInstances$.next(this.activeInstances);
      componentRef.destroy();
    };

    this.modalRef.push(componentRef);

    componentRef$.next(componentRef);
    componentRef$.complete();

    return <Observable<ComponentRef<T>>>componentRef$.asObservable();
  }



  /*
  create<T>(module: any, component: any, parameters?: Object): Observable<ComponentRef<T>> {
    this.bsModal.show();

    let componentRef$ = new ReplaySubject();
    this.compiler.compileModuleAndAllComponentsAsync(module).then((factory) => {
      let componentFactory = factory.componentFactories.filter(item => item.componentType === component)[0];
      const childInjector = ReflectiveInjector.resolveAndCreate([], this.injector);
      let componentRef = this.vcRef.createComponent(componentFactory, 0, childInjector);
      Object.assign(componentRef.instance, parameters); //pass the @Input parameters to the instance
      this.activeInstances ++;
      componentRef.instance["componentIndex"] = this.activeInstances;

      componentRef.instance["destroy"] = () => {
        alert('modal component destroying');
        this.activeInstances --;
        componentRef.destroy();
      };

      componentRef.onDestroy(()=>{
        alert('model component destroyed');
        this.activeInstances --;
        //componentRef.hostView.destroy();
      });
      //componentRef.changeDetectorRef.detectChanges();
      componentRef$.next(componentRef);
      componentRef$.complete();
    });
    //this.bsModal.show();
    return <Observable<ComponentRef<T>>> componentRef$.asObservable();
  }
*/
  //createFromFactory<T>(componentFactory: ComponentFactory<T>,
  //                     parameters?: Object): Observable<ComponentRef<T>> {
  //  let componentRef$ = new ReplaySubject();
  //  const childInjector = ReflectiveInjector.resolveAndCreate([], this.injector);
  //  let componentRef = this.vcRef.createComponent(componentFactory, 0, childInjector);
  //  // pass the @Input parameters to the instance
  //  Object.assign(componentRef.instance, parameters);
  //  this.activeInstances++;
  //  componentRef.instance["destroy"] = () => {
  //    this.activeInstances--;
  //    componentRef.destroy();
  //  };
  //  componentRef$.next(componentRef);
  //  componentRef$.complete();
  //  return componentRef$.asObservable();
  //}

}
