//reference http://blog.brecht.io/Modals-in-angular2/

import { NgModule } from '@angular/core';
import { ModalModule as BootstrapModalModule } from 'ng2-bootstrap/modal';

import { ModalPlaceholderComponent } from './modal-placeholder/modal-placeholder.component';
import { ModalService } from './modal.service';

import {PostAddEditComponent} from '../posts/post-add-edit/post-add-edit.component';
@NgModule({
  imports: [BootstrapModalModule],
  declarations: [ ModalPlaceholderComponent ],
  exports: [ ModalPlaceholderComponent, BootstrapModalModule ],
  //providers: [  ]
  providers: [ ModalService ],
  entryComponents: [ PostAddEditComponent ]
})
export class ModalModule { }
