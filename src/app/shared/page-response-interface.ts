/**
 * Created by ruban on 5/4/2017.
 */
export interface PageResponseInterface{
    total: number;
    current_page?: number;
}
