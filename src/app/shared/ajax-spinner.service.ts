/**
 * Created by ruban on 4/20/2017.
 */


export class AjaxSpinnerService {
    /**
     * Spinner Array
     * @type {Array}
     */
    protected spinners:Array<Spinner> = [];

    /**
     * Show spinner
     * @return {Spinner}
     */
    public showSpinner (container?:string):Spinner{
        let spinner:Spinner = new Spinner(container);
        spinner.show();
        this.spinners.push(spinner);
        return spinner;
    }
    public hideAll (){
        for(let i in this.spinners){
            this.spinners[i].hide();
        }
    }

}

export class Spinner  {
    protected element:HTMLDivElement;

    protected _html:string = `<div class="cssload-container">
	  <div class="cssload-whirlpool"></div>
  </div>`;

    protected parentContainer:HTMLElement = null;
    /**
     *
     * @param {string} container Where to put the spinner. Default to body
     */
    constructor(container?:string){
        this.parentContainer= document.getElementById(container);

        if(!this.parentContainer){
            this.parentContainer = document.body;
        }
        let className:string = this.parentContainer.className || '';
        className.replace('cssload-parent','');
        this.parentContainer.className = className + " cssload-parent";
    }

    private createSpinner (){

        this.element = document.createElement("div");
        this.element.className = "cssload-overlay";
        this.element.innerHTML = this._html;

        this.parentContainer.appendChild(this.element);

    }
    public show (){
        this.createSpinner();
    }

    public hide (){
        if(this.element){
            this.element.remove();
        }
        this.element = null;
    }
}