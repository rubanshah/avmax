import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class GeneralService {
    /**
     * Spinner Array
     * @type {Array}
     */
    protected countries:Array<Object> = [];

    constructor(){
        this.setCountries();
    }

    setCountries(){
        this.countries = [
                            {code:'USA',name:'USA'},
                            {code:'CA',name:'Canada'},
                            {code:'NP',name:'Nepal'}
        ];
    }

    getCountries(){
        return this.countries;
    }

    getCountryByCode(){

    }

    getCountryNameByCode(code){

        if(!code) return null;

        let country = _.find(this.getCountries(),['code',code]);
        if(country)
            {return country['name'];}
        else{
            return null;
        }
    }


}