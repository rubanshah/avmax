import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralService } from './general.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [GeneralService],
  exports: [CommonModule]
})
export class SharedModule { }
