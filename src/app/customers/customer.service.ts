import { Injectable } from '@angular/core';
import { RequestOptionsArgs } from '@angular/http';


import {RestService} from '../core/rest.service';
import {environment} from '../../environments/environment';

@Injectable()
export class CustomerService {
  protected resourceEndPoint = 'customers';

  constructor(protected http: RestService) { }

  getCustomers(options){
    let url = environment.API_URL + '/' + this.resourceEndPoint;
    options={search:options};
    return this.http.request(url,'get',options);
  }

  get(id){
    let url = environment.API_URL + '/' + this.resourceEndPoint+'/'+id;
    return this.http.getItem(url);
  }

  create(data){
    let url = environment.API_URL + '/' + this.resourceEndPoint;
    return this.http.createItem(url,data);
  }

  update(id,data){
    let url = environment.API_URL + '/' + this.resourceEndPoint+'/'+id;
    return this.http.patchItem(url,data);
  }

  getComment(id){
    let url = environment.API_URL + '/comments'+'/'+id;
    return this.http.getItem(url);
  }

  getCustomerComments(id){
    let url = environment.API_URL + '/' + this.resourceEndPoint+'/'+id+'/comments';
    return this.http.request(url,'get');

  }

  createComment(id,data){
    let url = environment.API_URL + '/' + this.resourceEndPoint+'/'+id+'/comments';
    return this.http.createItem(url,data);
  }

  deleteComment(id){
    let url = environment.API_URL + '/comments'+'/'+id;
    return this.http.request(url,'delete');
  }

  updateComment(id,data){
    let url = environment.API_URL + '/comments'+'/'+id;
    return this.http.patchItem(url,data);
  }

}
