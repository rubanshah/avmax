import { Component, OnInit, Output, Input, EventEmitter, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';
import { Observable }   from 'rxjs/Observable';

import { Comment } from '../model/comment.model';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { GeneralService } from '../../shared/general.service';
import { ModalService } from '../../modal/modal.service';
import { CustomerService } from '../customer.service';

import { CommentAddEditComponent } from '../comment-add-edit/comment-add-edit.component';

import { AppModule } from '../../app.module';

@Component({
    selector: 'app-customer-comment-list',
    templateUrl: './comment-list.component.html',
    styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {

    @Input()
        customerId: number; //input entity id that is customerid

    private subscriptions:Subscription[] = [];

    comments:Array<Object> = [];
    public commentListSubscription:Subscription;


    constructor(public generalService: GeneralService, public customerService: CustomerService, private modalService: ModalService, private componentFactoryResolver: ComponentFactoryResolver,  public toastr: ToastsManager,  vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }


    ngOnInit() {
        this.getComments();
    }

    getComments(){
       this.commentListSubscription = this.customerService.getCustomerComments(this.customerId).subscribe((d)=>{
            this.comments = d;
        },(e)=>{
            console.log(e);
        });
    }

    addComment(){
        let commentAddEditComponent = this.modalService.create(AppModule, CommentAddEditComponent,{customerId:this.customerId} ).subscribe((c)=> {
            c['_component'].cancel.subscribe(()=> {
                c.destroy();
                this.modalService.bsModal.hide();
            });

            c['_component'].save.subscribe(()=> {
                this.getComments();
                c.destroy();
                this.modalService.bsModal.hide();
            });
        });

        this.subscriptions.push(commentAddEditComponent);

    }

    edit(id){
        let commentAddEditComponent = this.modalService.create(AppModule, CommentAddEditComponent,{id:id} ).subscribe((c)=> {
            c['_component'].cancel.subscribe(()=> {
                c.destroy();
                this.modalService.bsModal.hide();
            });

            c['_component'].save.subscribe(()=> {
                this.getComments();
                c.destroy();
                this.modalService.bsModal.hide();
            });
        });

        this.subscriptions.push(commentAddEditComponent);
    }

    delete(id){
        if(!confirm("Are you sure to delete this comment?")) {return;}

        this.customerService.deleteComment(id).subscribe((d)=>{
            this.getComments();
            this.toastr.success('Comment deleted');
        },(e)=>{
            this.toastr.error("Oops Something went wrong");
        })
    }

    ngOnDestroy(){
    }


}
