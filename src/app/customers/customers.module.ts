import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ng2-bootstrap/modal';
import { TabsModule } from 'ng2-bootstrap/tabs'

import { BusyModule } from 'angular2-busy';

import { CustomerRoutingModule } from './customers-routing.module';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerAddEditComponent } from './customer-add-edit/customer-add-edit.component';
import { CustomerService } from './customer.service';
import { CustomerStatisticsComponent } from './customer-statistics/customer-statistics.component';

import { CommentListComponent } from './comment-list/comment-list.component';
import { CommentAddEditComponent } from './comment-add-edit/comment-add-edit.component';

import { SharedModule } from '../shared/shared.module';
import { GeneralService } from '../shared/general.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CustomerRoutingModule,
    NgxDatatableModule,
    ModalModule,
    BusyModule,
    TabsModule.forRoot(),
    SharedModule
  ],
  declarations: [CustomerListComponent, CustomerDetailComponent, CustomerAddEditComponent, CustomerStatisticsComponent, CommentAddEditComponent, CommentListComponent],
  providers: [ CustomerService, GeneralService ],
  exports: [CustomerAddEditComponent],
  entryComponents:[CustomerAddEditComponent, CommentAddEditComponent]
})
export class CustomersModule { }
