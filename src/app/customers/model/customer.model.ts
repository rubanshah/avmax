import { BaseEntity } from '../../shared/model/base-entity';
import { GeneralService } from '../../shared/general.service';
import * as _ from 'lodash';

export class Customer implements BaseEntity{

    id:number=null;
    code:number;
    name:string;
    email: string;
    phone: string;
    fax: string;
    address:string;
    country: string;
    city: string ;
    contact_person_name: string ;
    contact_person_email: string ;
    contact_person_phone: string ;

    constructor(obj=null){
        if(obj){
            this.initCustomer(obj);
        }
    }

    initCustomer(obj){
        this.id = obj.id;
        this.code = obj.code;
        this.name = obj.name;
        this.email = obj.email;
        this.phone = obj.phone;
        this.fax = obj.fax;
        this.address = obj.address;
        this.country = obj.country;
        this.city = obj.city;
        this.contact_person_name = obj.contact_person_name;
        this.contact_person_email = obj.contact_person_email;
        this.contact_person_phone = obj.contact_person_phone;
    }

    getAddress(){
        let data = [];
        if(this.address){data.push(this.address)}
        if(this.city){data.push(this.city)}
        if(this.country){data.push(this.country)}

        return data.join(', ');
    }


}