import { BaseEntity } from '../../shared/model/base-entity';
import { GeneralService } from '../../shared/general.service';
import * as _ from 'lodash';

export class Comment implements BaseEntity{

    id:number=null;
    user_id:number;
    customer_id:number;
    customer_code:string;
    text: string;

    constructor(obj=null){
        if(obj){
            this.initComment(obj);
        }
    }

    initComment(obj){
        this.id = obj.id;
        this.user_id = obj.user_id;
        this.customer_id = obj.customer_id;
        this.customer_code = obj.customer_code;
        this.text = obj.text;
    }

}