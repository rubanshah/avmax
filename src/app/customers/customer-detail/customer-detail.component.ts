import { Component, OnInit, OnDestroy, ComponentFactoryResolver } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription }   from 'rxjs/Subscription';
import { Observable }   from 'rxjs/Observable';

import { ModalService } from '../../modal/modal.service';
import { CustomerAddEditComponent } from '../customer-add-edit/customer-add-edit.component';

import {BusyDirective} from 'angular2-busy';

import { AppModule } from '../../app.module';
import { CustomerService } from '../customer.service';
import { Customer } from '../model/customer.model';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css'],
})
export class CustomerDetailComponent implements OnInit, OnDestroy {

  private subscriptions:Subscription[] = [];
  public id: number;
  public customer:Customer;
  public customerSubscription:Subscription;


  constructor( public route: ActivatedRoute,  protected customerService: CustomerService, private modalService: ModalService, private componentFactoryResolver: ComponentFactoryResolver) {

  }

  ngOnInit() {
    this.route.params.subscribe((p)=>{
      this.id = p.id;
      this.getCustomer();
    });
  }

  getCustomer(){
    this.customerSubscription = this.customerService.get(this.id).subscribe((d)=> {
      this.customer = new Customer(d);
    }, (e)=> {
      console.log(e);
    });
  }

   edit(){
    let customerAddEditComponent = this.modalService.create(AppModule, CustomerAddEditComponent, {id:this.id}).subscribe((c)=> {
      c['_component'].cancel.subscribe(()=> {
        c.destroy();
        this.modalService.bsModal.hide();
      });

      c['_component'].save.subscribe(()=> {
        this.getCustomer();
        c.destroy();
        this.modalService.bsModal.hide();
      });
    });
    this.subscriptions.push(customerAddEditComponent);
  }

  ngOnDestroy(){
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
