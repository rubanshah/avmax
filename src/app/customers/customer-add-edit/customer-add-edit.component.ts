import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { GeneralService } from '../../shared/general.service';
import { CustomerService } from '../customer.service';
import { Customer } from '../model/customer.model';
import { Subscription }   from 'rxjs/Subscription';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({
  selector: 'app-customer-add-edit',
  templateUrl: './customer-add-edit.component.html',
  styleUrls: ['./customer-add-edit.component.css']
})
export class CustomerAddEditComponent implements OnInit {

  @Output()
      cancel: EventEmitter<any> = new EventEmitter();

  @Output()
      save: EventEmitter<any> = new EventEmitter();

  @Input()
      id: number; //input entity id that is customerid

  customer:Customer;

  formSaveSubscripton:Subscription;
  customerSubscription:Subscription;

  formErrors:any;

  constructor(public generalService: GeneralService, public customerService: CustomerService, public toastr: ToastsManager) {
  }


  ngOnInit() {
    this.customer = new Customer();
    if(this.id){
      this.getCustomer();
    }
  }

  getCustomer(){
    this.customerSubscription = this.customerService.get(this.id).subscribe((d)=>{
      this.customer = new Customer(d);
      console.log(this.customer);
    },(e)=>{
      console.log(e);
    });
  }

  create(){
    this.formSaveSubscripton = this.customerService.create(this.customer).subscribe((d)=>{
      this.save.emit();
      this.toastr.success("New Customer Added");

    },(e)=>{
      if(e.status==422){
        this.toastr.error("Please, correct form errors");
        this.formErrors=JSON.parse(e._body);
        return;
      }
      this.toastr.error("Oops Something went wrong");
    })
  }

  update(){
    this.formSaveSubscripton = this.customerService.update(this.id,this.customer).subscribe((d)=>{
      this.save.emit();
      this.toastr.success("Customer Modified");

    },(e)=>{
      //console.log(e);
      if(e.status.httpStatus==422){
        this.toastr.error("Please, correct form errors");
        this.formErrors=e.errors;
        //console.log(this.formErrors);
        return;
      }
      this.toastr.error("Oops Something went wrong");
    })
  }


  formCancel(){
    this.cancel.emit();
  }

  formSave(){
    if(this.id){
      this.update();
    }else{
      this.create();
    }
  }

  ngOnDestroy(){
  }


}
