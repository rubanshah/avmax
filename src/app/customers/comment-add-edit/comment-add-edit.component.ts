import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { GeneralService } from '../../shared/general.service';
import { CustomerService } from '../customer.service';
import { Comment } from '../model/comment.model';


@Component({
    selector: 'app-comment-add-edit',
    templateUrl: './comment-add-edit.component.html',
    styleUrls: ['./comment-add-edit.component.css']
})
export class CommentAddEditComponent implements OnInit {

    @Output()
        cancel: EventEmitter<any> = new EventEmitter();

    @Output()
        save: EventEmitter<any> = new EventEmitter();

    @Input()
        id: number; //input entity id that is customerid

    @Input()
        customerId: number;

    comment:Comment;

    commentSubscription:Subscription;
    formSaveSubscription:Subscription;
    formErrors:any;


    constructor(public generalService: GeneralService, public customerService: CustomerService, public toastr: ToastsManager) {
    }


    ngOnInit() {
        this.comment = new Comment();
        if(this.id){
            this.getComment();
        }
    }

    getComment(){
      this.commentSubscription =  this.customerService.getComment(this.id).subscribe((d)=>{
            this.comment = new Comment(d);
        },(e)=>{
            this.toastr.error("Oops Something went wrong");
        });
    }

    formSave(){
        if(this.id){
            this.update();
        }else{
            this.create();
        }
    }

    create(){
        this.formSaveSubscription = this.customerService.createComment(this.customerId,this.comment).subscribe((d)=>{
            this.save.emit();
            this.toastr.success("New Comment Added");

        },(e)=>{
            if(e.status.httpStatus==422){
                this.toastr.error("Please, correct form errors");
                this.formErrors=e.errors;
                return;
            }
            this.toastr.error("Oops Something went wrong");
        })
    }

    update(){
       this.formSaveSubscription = this.customerService.updateComment(this.id,this.comment).subscribe((d)=>{
            this.save.emit();
            this.toastr.success("Comment Modified");

        },(e)=>{
           if(e.status.httpStatus==422){
               this.toastr.error("Please, correct form errors");
               this.formErrors=e.errors;
               return;
           }
            this.toastr.error("Oops Something went wrong");
        })
    }


    formCancel(){
        this.cancel.emit();
    }

    ngOnDestroy(){
    }


}
