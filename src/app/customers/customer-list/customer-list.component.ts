import { Component, OnInit, ViewChild, AfterViewInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

import { DatatableComponent } from "@swimlane/ngx-datatable";

import { CustomerService } from '../customer.service';
import { Page, PageResponseInterface } from '../../shared/page';
import { Customer } from '../model/customer.model';

import 'rxjs/add/operator/debounceTime';


@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  columns:Array<any> = [];
  customers:Array<any> = [];

  page:Page = new Page();

  options = {page:0,search:''};
  loadingIndicator: boolean = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('addressCell') public addressCell: TemplateRef<any>;


  constructor(private router: Router, private customerService: CustomerService) { }

  ngOnInit() {

    this.columns = [
      { prop: 'code', maxWidth:70 },
      { prop: 'name' },
      { prop: 'phone', maxWidth:130 },
      { name: 'Address',cellTemplate: this.addressCell  },
      { prop: 'contact_person_name', name:'Contact Person' }
    ];

    this.setPage({offset:0});

}


  getCustomers(){
    this.loadingIndicator = true;
    this.customerService.getCustomers(this.options).subscribe((d)=>{

      //create customer object
      this.customers = [];
      for(let i=0;i<d.data.length;i++){
        this.customers.push(new Customer(d.data[i]));
      }
      //eof


      this.handlePagination(d);
      this.loadingIndicator =false;
    });
  }

  clearFilter(){
    this.options.search='';
    this.getCustomers();
  }

  handleRowClick($event){
    let customerId = $event.row.id;
    this.router.navigate(['/customers', customerId]);
  }


  setPage(pageInfo){
    this.page.pageNumber = pageInfo.offset;
    this.options.page = this.page.pageNumber+1;
    this.getCustomers();
  }

  handlePagination(d:PageResponseInterface){
    this.page.totalElements = d.total;
    this.table.recalculate();
  }

}
