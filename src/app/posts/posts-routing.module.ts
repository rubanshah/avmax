import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from '../layout/layout.component';

import {PostListComponent} from './post-list/post-list.component';

const postsRoutes: Routes = [

  { path: '', component: LayoutComponent, children: [
    { path: 'posts', component: PostListComponent },
  ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(postsRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class PostsRoutingModule { }
