import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, ComponentFactoryResolver } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription }   from 'rxjs/Subscription';

import { DatatableComponent } from "@swimlane/ngx-datatable";
import { AppModule } from '../../app.module';


import { ModalService } from '../../modal/modal.service';
import { PostService } from '../post.service';
import { Page, PageResponseInterface } from '../../shared/page';

import { PostAddEditComponent } from '../post-add-edit/post-add-edit.component';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {

  private subscriptions:Subscription[] = [];

  columns:Array<any> = [];
  posts:Array<any> = [];

  page:Page = new Page();

  options = {page:0};
  loadingIndicator: boolean = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router, private postService: PostService, private modalService: ModalService, private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {

    this.columns = [
      { prop: 'title' },
      { prop: 'slug' }
    ];

    this.setPage({offset:0});

  }

  getUsers(){
    this.loadingIndicator = true;
    this.postService.getPosts(this.options).subscribe((d)=>{

      this.posts = d.data;

      this.handlePagination(d);
      this.loadingIndicator =false;
    });
  }

  handleRowClick($event){
    let id = $event.row.id;
    this.edit(id);
  }


  setPage(pageInfo){
    this.page.pageNumber = pageInfo.offset;
    this.options.page = this.page.pageNumber+1;
    this.getUsers();
  }

  handlePagination(d:PageResponseInterface){
    this.page.totalElements = d.total;
    this.table.recalculate();
  }



  addNew(){
    let postAddEditComponent = this.modalService.create(AppModule, PostAddEditComponent).subscribe((c)=> {
      console.log(c);
      c['_component'].cancel.subscribe(()=> {
        //alert('cancel');
        c.destroy();
        this.modalService.bsModal.hide();
      });
      //c['instance'].cancel.subscribe(()=>{c.destroy();})
    });
    this.subscriptions.push(postAddEditComponent);
    //console.log(userAddEditComponent);
  }

  edit(id){
    let postAddEditComponent = this.modalService.create(AppModule, PostAddEditComponent, {id:id}).subscribe((c)=> {
      console.log(c);
      c['_component'].cancel.subscribe(()=> {
        //alert('cancel');
        c.destroy();
        this.modalService.bsModal.hide();
      });
      //c['instance'].cancel.subscribe(()=>{c.destroy();})
    });
    this.subscriptions.push(postAddEditComponent);
    //console.log(userAddEditComponent);
  }

  ngOnDestroy(){
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
