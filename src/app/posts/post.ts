enum type {page,post};

export class Post {
    slug: string;
    title: any = {};
    body:  any = {};
    excerpt: any = {};
    seo_title: any;
    seo_meta_key: any;
    seo_meta_description: any;
    type:type;
    status:number;

    public addLanguage(){

        this.title['en']='';
        this.body['en']='';
        this.excerpt['en']='';

        //remainig for seo
    }
}