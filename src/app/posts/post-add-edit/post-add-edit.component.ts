import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from '../post';
import { PostService } from '../post.service';


@Component({
  selector: 'app-post-add-edit',
  templateUrl: './post-add-edit.component.html',
  styleUrls: ['./post-add-edit.component.css']
})
export class PostAddEditComponent implements OnInit {

  @Output()
  cancel: EventEmitter<any> = new EventEmitter();

  @Input()
  id: number;

  post: Post;

  constructor(protected postService: PostService) {
    alert('constructor call');
  }

  ngOnInit() {
    alert('init function call');

    if(this.id){
      this.post = new Post();
      this.getPost(this.id);
    }else{
      this.post = new Post();
      this.post.addLanguage();
    }
    console.log(this.post);
  }

  getPost(id){
    this.postService.get(id).subscribe((d)=>{
      this.post=d;
    },(e)=>{

    });
  }

  formCancel(){
    this.cancel.emit();
  }

  save(){
    console.log(this.post);
    if(this.post['id']){
      this.update();
    }else{
      this.create();
    }
  }

  create(){
    this.postService.create(this.post).subscribe((d)=>{
      alert('success');
    },(e)=>{
      alert('failed');
    });
  }

  update(){
    this.postService.update(this.post).subscribe((d)=>{
      alert('success');
    },(e)=>{
      alert('failed');
    });
  }

  ngOnDestroy(){
    alert('ngondestroy call');
  }

}
