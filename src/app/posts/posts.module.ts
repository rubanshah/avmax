import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CKEditorModule } from 'ng2-ckeditor';

import { PostsRoutingModule } from './posts-routing.module';
import { PostListComponent } from './post-list/post-list.component';
import { PostAddEditComponent } from './post-add-edit/post-add-edit.component';

import { PostService } from './post.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    CKEditorModule,
    PostsRoutingModule
  ],
  declarations: [PostListComponent, PostAddEditComponent],
  providers: [PostService],
  exports:[PostAddEditComponent]
})
export class PostsModule { }
