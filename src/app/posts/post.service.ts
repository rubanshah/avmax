import { Injectable } from '@angular/core';
import { RequestOptionsArgs } from '@angular/http';


import {RestService} from '../core/rest.service';
import {environment} from '../../environments/environment';

@Injectable()
export class PostService {
  protected resourceEndPoint = 'posts';

  constructor(protected http: RestService) { }

  getPosts(options){
    let url = environment.API_URL + '/' + this.resourceEndPoint;
    options={search:options};
    return this.http.request(url,'get',options);
  }

  get(id){
    let url = environment.API_URL + '/' + this.resourceEndPoint+'/'+id;
    return this.http.getItem(url);
  }

  create(post){
    let url = environment.API_URL + '/' + this.resourceEndPoint;
    return this.http.createItem(url,post);
  }

  update(post){
    let url = environment.API_URL + '/' + this.resourceEndPoint+'/'+post.id;
    return this.http.patchItem(url,post);
  }

}
