export const environment = {
  production: true,
  APP_BASE_URL: 'http://demo.zenixmedia.com/avmax/html',
  API_BASE_URL: 'http://demo.zenixmedia.com/avmax/api/public',
  API_URL: 'http://demo.zenixmedia.com/avmax/api/public/api'
};
